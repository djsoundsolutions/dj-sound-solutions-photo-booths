import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import styled from "styled-components"
import Navbar from "../components/navbar"
import Footer from "../components/footer"

const Container = styled.div`
  margin: 0px;
  padding: 50px;
  text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.3);
  .que {
    color: #724bb7;
    font-size: 22px;
    padding-top: 25px;
  }
`

const faqPage = () => {
  return (
    <Layout>
      <SEO
        title="FAQs"
        description="DJ Sound Solutions & Photo Booths - Frequently Asked Questions"
      />
      <Navbar />
      <Container>
        <p className="que">How do I pay the deposit?</p>
        <p>
          The deposit can be paid by cash, check, money order, PayPal or
          Facebook's payment feature. Once you decide that you want to book with
          us, then you can put down a deposit to reserve your event date.
        </p>
        <p className="que">When is the remaining balance due?</p>
        <p>
          The remaining balance is due 30 days prior to the date of your event.
        </p>
        <p className="que">Can people request songs during an event?</p>
        <p>Absolutely. People can request as many songs as they wish.</p>
        <p className="que">Do you have back-up equipment?</p>
        <p>
          Absolutely. We have a back-up system as well as a back-up DJ and Photo
          Booth attendant.
        </p>
        <p className="que">
          What does the banquet hall need to provide you with?
        </p>
        <p>
          All the banquet hall needs to provide us with is an electrical outlet.
        </p>
        <p className="que">How many types of lights do you have?</p>
        <p>
          We have a variety of top-of-the-line lights. We have led colored
          uplighting, colored lights and best of all.. laser lights!
        </p>
        <p className="que">
          How can I find out if you're available for my event?
        </p>
        <p>
          <a href="/#contact" style={{ color: "#ff6584", paddingRight: 5 }}>
            Contact us,
          </a>
          and we'll check to see if we're available for your event date.
        </p>

        <p className="que">When should I book my date?</p>
        <p>
          It is recommended to book with us as soon as you know your event date.
          We have limited availability, and we book up fast.
        </p>
        <p className="que">How do I book DJ Sound Solutions' services?</p>
        <p>
          By putting down a deposit and signing a booking agreement to secure
          the event date. You can reach us via phone, email or our built-in
          form.
          <span style={{ paddingLeft: 5 }}>
            <a href="/#contact" style={{ color: "#ff6584" }}>
              View contact details
            </a>
          </span>
        </p>
        <p className="que">How big is the photo booth?</p>
        <p>
          Approximately 8 feet tall. We would need a 10 - 12 foot space for
          setup.
        </p>
      </Container>
      <Footer />
    </Layout>
  )
}

export default faqPage
