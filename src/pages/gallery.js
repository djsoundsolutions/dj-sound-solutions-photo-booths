import React from "react"
import { graphql } from "gatsby"
import Img from "gatsby-image"
import Layout from "../components/layout"
import SEO from "../components/seo"
import styled from "styled-components"
import Navbar from "../components/navbar"
import Footer from "../components/footer"

// Styled-Components Start

const Container = styled.section`
  margin: 0px;
  padding: 50px;
  width: 100%;
  height: 100%;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  @media screen and (max-width: 768px) {
    padding: 0px 0px 25px 0px;
  }
  .galleryImageDiv {
    width: 300px;
    height: 300px;
    @media screen and (max-width: 768px) {
      margin-bottom: 5px;
      width: 100%;
      height: auto;
    }
    .galleryImg {
      width: 100%;
      height: 100%;
    }
  }
`

const GalleryHeader = styled.h3`
  margin: 0px auto 0px 70px;
  padding-top: 100px;
  color: #724bb7;
  font-size: 42px;
  text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.5);
  @media screen and (min-width: 1001px) and (max-width: 1300px) {
    margin: 0px auto 0px 38px;
  }
  @media screen and (min-width: 769px) and (max-width: 1000px) {
    margin: 0px;
    padding-top: 125px;
    width: 100%;
    text-align: center;
  }
  @media screen and (min-width: 700px) and (max-width: 768px) {
    margin: 0px;
    padding-top: 125px;
    padding-bottom: 50px;
    width: 100%;
    text-align: center;
  }
  @media screen and (min-width: 401px) and (max-width: 699px) {
    margin: 0px;
    padding-top: 125px;
    padding-bottom: 50px;
    width: 100%;
    text-align: center;
    font-size: 2rem;
  }
  @media screen and (max-width: 400px) {
    margin: 0px;
    padding-top: 125px;
    padding-bottom: 50px;
    width: 100%;
    text-align: center;
    font-size: 1.5rem;
  }
`

// Styled-Components End

const Gallery = ({ data }) => {
  return (
    <Layout>
      <SEO
        title="Gallery"
        description="DJ Sound Solutions & Photo Booths - Photo Gallery"
      />
      <Navbar />
      <GalleryHeader>It's going to be amazing...</GalleryHeader>
      <Container>
        {data.allContentfulGalleryImage.edges.map(edge => (
          <div key={edge.node.photo.id} className="galleryImageDiv">
            <Img
              className="galleryImg"
              alt={edge.node.photo.description}
              fluid={edge.node.photo.fluid}
              key={edge.node.photo.id}
            />
          </div>
        ))}
      </Container>
      <Footer />
    </Layout>
  )
}

export const query = graphql`
  {
    allContentfulGalleryImage {
      edges {
        node {
          photo {
            id
            description
            fluid {
              ...GatsbyContentfulFluid
            }
            file {
              url
              fileName
              contentType
            }
          }
        }
      }
    }
  }
`

export default Gallery
