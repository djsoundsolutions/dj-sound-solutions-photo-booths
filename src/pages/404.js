import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"

const NotFoundPage = () => (
  <Layout>
    <SEO title="404: Not Found" />
    <div
      style={{
        marginTop: "5%",
        width: "100%",
        height: "100%",
      }}
    >
      <h1 style={{ textAlign: "center", fontSize: "3rem" }}>NOT FOUND</h1>
      <p style={{ textAlign: "center", fontSize: "2rem" }}>
        You just hit a route that doesn&#39;t exist...
      </p>
    </div>
  </Layout>
)

export default NotFoundPage
