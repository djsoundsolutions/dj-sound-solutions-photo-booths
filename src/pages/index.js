import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import HeroImage from "../components/heroImage"
import Navbar from "../components/navbar"
import Benefits from "../components/benefits"
import Packages from "../components/packages"
import Extras from "../components/extras"
import PhotoBooth from "../components/photoBooth"
import Cake from "../components/cake"
import Testimonials from "../components/testimonials"
import Contact from "../components/contact"
import Footer from "../components/footer"

const IndexPage = () => (
  <Layout>
    <SEO
      title="Home"
      description="Don’t leave music to chance when it comes to your wedding or special event. We've served over 1,000 weddings and celebrations, and we can’t wait
      to make your next big day an epic experience for you and your guests to enjoy.
      DJ Sound Solutions is an award winning, full-time DJ, lighting and photo booth company that serves all of Florida, Indiana, Wisconsin and Illinois
      "
      keywords={[
        `dj`,
        `party dj`,
        `dj rental for wedding`,
        `dj hire near me`,
        `hire dj for event`,
        `photo booth`,
        `photo booth rental`,
        `photo booth hire`,
        `wedding photo booth`,
      ]}
    />
    <Navbar />
    <HeroImage />
    <Benefits />
    <Packages />
    <Extras />
    <PhotoBooth />
    <Cake />
    <Testimonials />
    <Contact />
    <Footer />
  </Layout>
)

export default IndexPage
