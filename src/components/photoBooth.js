import React from "react"
import styled from "styled-components"
import PhotoBoothImg from "../components/photoBoothImg"

// Styled-Components Start

const Container = styled.section`
  margin: 0px;
  padding: 0px;
  width: 100%;
  .photoBoothText {
    margin-left: 100px;
    padding: 0px 10px;
    width: 800px;
    position: relative;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    h2 {
      margin: 0px;
      padding-bottom: 25px;
      color: #724bb7;
      font-size: 42px;
      text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.5);
      @media screen and (min-width: 1390px) and (max-width: 1663px) {
        text-align: center;
      }
      @media screen and (min-width: 482px) and (max-width: 1389px) {
        font-size: 2.125rem;
        text-align: center;
      }
      @media screen and (max-width: 481px) {
        font-size: 1.5rem;
        text-align: center;
      }
    }
    p {
      width: 100%;
      font-size: 32px;
      color: #111;
      text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.3);
      @media screen and (min-width: 1390px) and (max-width: 1663px) {
        margin: 0px;
        text-align: center;
      }
      @media screen and (min-width: 777px) and (max-width: 1389px) {
        margin: 0px;
        padding: 0px 100px;
        font-size: 1.5rem;
        text-align: center;
      }
      @media screen and (min-width: 482px) and (max-width: 776px) {
        margin: 0px;
        padding: 0px 25px;
        font-size: 1.2rem;
        text-align: center;
      }
      @media screen and (max-width: 481px) {
        margin: 0px;
        padding: 0px;
        font-size: 0.9rem;
        text-align: center;
      }
    }
    @media screen and (min-width: 1390px) and (max-width: 1663px) {
      margin: 100px 0px 50px 0px;
      width: 1100px;
    }
    @media screen and (max-width: 1389px) {
      margin: 100px 0px 50px 0px;
      width: 100%;
    }
  }
`

const SubContainer = styled.div`
  margin: 0px;
  padding: 100px 50px 150px 60px;
  width: 100%;
  position: relative;
  display: flex;
  justify-content: flext-start;
  align-items: flex-start;
  flex-wrap: wrap;
  .photoBooth {
    width: 650px;
    position: relative;
    @media screen and (min-width: 851px) and (max-width: 1663px) {
      width: 800px;
    }
    @media screen and (max-width: 850px) {
      width: 95%;
    }
  }
  .photoBoothImg {
    margin-top: 10px;
    position: relative;
    border-radius: 3px;
    box-shadow: 0px 0px 3px 5px rgba(0, 0, 0, 0.1),
      0px 0px 5px 1.25px rgba(0, 0, 0, 0.5);
    @media screen and (max-width: 1663px) {
      margin-top: 0px;
    }
  }
  @media screen and (min-width: 1390px) and (max-width: 1663px) {
    padding: 100px 50px 100px 50px;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
  }
  @media screen and (min-width: 415px) and (max-width: 1389px) {
    padding: 100px 0px;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
  }
  @media screen and (max-width: 414px) {
    padding: 100px 0px 0px 0px;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
  }
`

// Styled-Components End

const PhotoBooth = () => {
  return (
    <Container id="photobooth">
      <SubContainer>
        <PhotoBoothImg />
        <div className="photoBoothText">
          <h2>Photo Booth for all occasions!</h2>
          <p>
            Whether you’re planning a wedding, quinceañera, sweet sixteen,
            corporate event, anniversary or any other special event, a photo
            booth is a really fun way to preserve memories and entertain your
            guests.
            <br />
            <a href="#contact" style={{ color: "#ff6584" }}>
              Contact us
            </a>{" "}
            for more information.
          </p>
        </div>
      </SubContainer>
    </Container>
  )
}

export default PhotoBooth
