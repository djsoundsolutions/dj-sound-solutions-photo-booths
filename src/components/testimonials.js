import React, { useState } from "react"
import { animated, useSpring, config } from "react-spring"
import { Waypoint } from "react-waypoint"
import styled from "styled-components"

// Styled-Components Start

const ReviewContainer = styled.div`
  margin: 0px 0px 25px 0px;
  padding: 0px;
  width: 100%;
  position: relative;
  display: flex;
  justify-content: space-around;
  align-items: flex-start;
  @media screen and (max-width: 1640px) {
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
  }
  @media screen and (max-width: 600px) {
    width: 100vw;
  }
`
const Review = styled.div`
  margin: 75px 20px 50px 20px;
  padding: 0px;
  width: 500px;
  p {
    text-align: center;
    color: #111;
    line-height: 1.5rem;
    text-shadow: 0.75px 0.75px 0.75px rgba(0, 0, 0, 0.3);
    @media screen and (max-width: 500px) {
      text-align: left;
    }
  }
  @media screen and (max-width: 1640px) {
    margin: 50px 0px;
  }
  @media screen and (max-width: 600px) {
    width: 90vw;
    padding: 10px;
  }
`
const ReviewHeader = styled.div`
  padding: 30px 0px;
  display: flex;
  justify-content: center;
  align-items: center;
  h3 {
    margin: 0px;
    padding: 0px;
    font-size: 42px;
    color: #724bb7;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.5);
    @media screen and (min-width: 601px) and (max-width: 900px) {
      font-size: 1.75rem;
    }
    @media screen and (max-width: 600px) {
      font-size: 1.25rem;
      text-align: center;
    }
  }
  @media screen and (max-width: 600px) {
    padding: 15px 10px;
  }
`

const HzRule = styled.hr`
  margin: 0px auto;
  width: 50%;
  height: 3px;
  border: none;
  border-radius: 3px;
  background: #ff6584;
  @media screen and (min-width: 601px) and (max-width: 1440px) {
    width: 80%;
  }
  @media screen and (min-width: 401px) and (max-width: 600px) {
    width: 90%;
  }
  @media screen and (max-width: 400px) {
    width: 95%;
  }
`

// Styled-Components End

const Testimonials = () => {
  const [on, toggle] = useState(false)
  const animation = useSpring({
    opacity: on ? 1 : 0,
    config: config.slow,
  })
  return (
    <section>
      <Waypoint
        bottomOffset="20%"
        onEnter={() => {
          if (!on) toggle(true)
        }}
      >
        <animated.div style={animation}>
          <ReviewHeader>
            <h3>Over 1,000 Events and Counting!</h3>
          </ReviewHeader>
          <HzRule />
        </animated.div>
      </Waypoint>
      <ReviewContainer>
        {/* Review one */}
        <Review>
          <p>
            We felt taken care of from day one! Shawn and Christina did a great
            job communicating with us on what we envisioned for our special day.
            Their services are affordable, which means SO much when you're
            already spending a lot to get married! They enlisted DJ Sparky to
            help-out for our event, and he did a fantastic job with keeping
            everyone entertained. Thank you!
          </p>
          <b />
          <b />
          <p>-Aimee</p>
        </Review>
        {/* Review two */}
        <Review>
          <p>
            A huge hit at our wedding on October 6th! Everyone loved it. Bri was
            our attendant, and everyone loved her! The props were amazing, the
            setup was perfect, and the booth was fantastic! Lighting inside was
            perfect and absolutely amazing! I am so glad that we went with them!
            Highly recommended! We loved going through all the pictures we
            received! We also hired our d.j. services through them and they were
            absolutely amazing as well. Christina, thank you thank you thank
            you!
          </p>
          <b />
          <b />
          <p>-Rachel</p>
        </Review>
        {/* Review three */}
        <Review>
          <p>
            Sparky and his wife went OUT of their way to make my wife and I a
            day that we will never forget. Quality sound, great interaction and
            just making our wedding fun. You want a fun atmosphere and a joyful
            experience? DJ Sparky is your guy!
          </p>
          <b />
          <b />
          <p>-Thomas</p>
        </Review>
      </ReviewContainer>
    </section>
  )
}

export default Testimonials
