import React from "react"
import { StaticQuery, graphql } from "gatsby"
import CTA from "../components/callToAction"
import styled from "styled-components"
import Img from "gatsby-image"

// Styled-Components Start

const Container = styled.section`
  margin: 0px;
  padding: 75px 20px 135px 20px;
  width: 100%;
  height: 1250px;
  position: relative;
  background-color: #ffffff;
  background-image: url("data:image/svg+xml,%3Csvg width='84' height='48' viewBox='0 0 84 48' xmlns='http://www.w3.org/2000/svg'%3E%3Cpath d='M0 0h12v6H0V0zm28 8h12v6H28V8zm14-8h12v6H42V0zm14 0h12v6H56V0zm0 8h12v6H56V8zM42 8h12v6H42V8zm0 16h12v6H42v-6zm14-8h12v6H56v-6zm14 0h12v6H70v-6zm0-16h12v6H70V0zM28 32h12v6H28v-6zM14 16h12v6H14v-6zM0 24h12v6H0v-6zm0 8h12v6H0v-6zm14 0h12v6H14v-6zm14 8h12v6H28v-6zm-14 0h12v6H14v-6zm28 0h12v6H42v-6zm14-8h12v6H56v-6zm0-8h12v6H56v-6zm14 8h12v6H70v-6zm0 8h12v6H70v-6zM14 24h12v6H14v-6zm14-8h12v6H28v-6zM14 8h12v6H14V8zM0 8h12v6H0V8z' fill='%23724bb7' fill-opacity='0.03' fill-rule='evenodd'/%3E%3C/svg%3E");
  .ctaPositioner {
    margin: 400px 0px 0px calc(48% - 400px);
    padding: 0px;
    position: absolute;
    width: 800px;
    z-index: 1;
    @media screen and (min-width: 701px) and (max-width: 900px) {
      margin: 220px 0px 0px calc(47% - 300px);
      width: 600px;
    }
    @media screen and (max-width: 700px) {
      margin: 0px;
      width: 100%;
      height: 100%;
      position: relative;
    }
  }
  @media screen and (min-width: 1001px) and (max-width: 1030px) {
    padding: 35px 20px 35px 20px;
  }
  @media screen and (min-width: 901px) and (max-width: 1000px) {
    padding: 100px 20px 35px 20px;
  }
  @media screen and (min-width: 701px) and (max-width: 900px) {
    padding: 100px 20px 35px 20px;
    height: 700px;
  }
  @media screen and (max-width: 700px) {
    padding: 0px;
    height: 500px;
  }
`

const ImgDiv = styled.div`
  margin: 0px auto;
  padding: 0px;
  height: 100%;
  border-radius: 3px;
  box-shadow: 0px 0px 3px 5px rgba(0, 0, 0, 0.1),
    0px 0px 5px 1.25px rgba(0, 0, 0, 0.5);
  @media screen and (max-width: 700px) {
    display: none;
  }
`

// Styled-Components End

const HeroImage = () => (
  <StaticQuery
    query={graphql`
      query {
        placeholderImage: file(relativePath: { eq: "dance.jpg" }) {
          childImageSharp {
            fluid(maxWidth: 3000) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    `}
    render={data => (
      <Container>
        <div className="ctaPositioner">
          <CTA />
        </div>
        <ImgDiv>
          <Img
            style={{
              position: "relative",
              height: "100%",
              width: "100%",
              borderRadius: 3,
            }}
            fluid={data.placeholderImage.childImageSharp.fluid}
            alt="Bride and groom's first dance. Photo by Skye Studios on Unsplash"
          />
        </ImgDiv>
      </Container>
    )}
  />
)

export default HeroImage
