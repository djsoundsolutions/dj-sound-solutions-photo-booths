import React, { useState } from "react"
import { useSpring, animated, config } from "react-spring"
import { Waypoint } from "react-waypoint"
import styled from "styled-components"
import Sparky from "./sparky"
import { FaFacebook } from "react-icons/fa"
import { IconContext } from "react-icons"
import Flatpickr from "react-flatpickr"
import "flatpickr/dist/themes/dark.css"

// Styled-Components Start

const Container = styled.section`
  width: 100%;
  margin: 0px;
  padding: 0px;
  position: relative;
  @media screen and (max-width: 700px) {
    padding-top: 100px;
  }
  .detailsFlexControl {
    margin: 0px;
    padding: 0px;
    width: 100%;
    position: relative;
    display: flex;
    justify-content: space-around;
    align-items: flex-start;
    flex-wrap: wrap;
    @media screen and (max-width: 1566px) {
      display: block;
    }
  }
`

const Banner = styled.div`
  margin: 0px 0px 100px 0px;
  padding: 0px;
  p {
    margin: 0px auto;
    display: block;
    width: 70%;
    font-size: 2rem;
    text-align: center;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.3);
    @media screen and (min-width: 451px) and (max-width: 835px) {
      padding: 0px 20px;
      width: 100%;
      font-size: 1.75rem;
    }
    @media screen and (max-width: 450px) {
      padding: 0px 10px;
      width: 100%;
      font-size: 1rem;
    }
  }
  .header {
    padding-bottom: 25px;
    width: 80%;
    font-size: 2.625rem;
    font-style: italic;
    color: #724bb7;
    @media screen and (min-width: 451px) and (max-width: 835px) {
      padding: 0px 20px 25px 20px;
      width: 100%;
      font-size: 2rem;
    }
    @media screen and (max-width: 450px) {
      padding: 0px 10px 25px 10px;
      width: 100%;
      font-size: 1.175rem;
    }
  }
  .travelFee {
    font-size: 1.25rem;
    font-style: italic;
    @media screen and (min-width: 451px) and (max-width: 700px) {
      font-size: 1rem;
    }
    @media screen and (max-width: 450px) {
      font-size: 0.75rem;
    }
  }
  .areaServed {
    padding-bottom: 25px;
  }
  @media screen and (max-width: 450px) {
    margin: 0px;
  }
`

const ContactContainer = styled.div`
  padding: 50px;
  min-width: 800px;
  max-width: 800px;
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  button {
    margin: 0px;
    padding: 15px;
    width: 225px;
    border: none;
    border-radius: 3px;
    background: #724bb7;
    color: #fff;
    text-shadow: 1px 1px 2px hsla(0, 0%, 0%, 0.9);
    font-size: 20px;
    letter-spacing: 1.1px;
    :hover {
      animation: sendAnimation 0.2s forwards;
    }
    @media screen and (max-width: 1566px) {
      margin-left: calc(50% - 112.5px);
    }
  }
  @media screen and (min-width: 801px) and (max-width: 1566px) {
    margin: 0px auto;
  }
  @media screen and (max-width: 800px) {
    padding: 35px 0px;
    min-width: 0px;
    width: 100%;
    button {
      width: 112.5px;
      margin-left: calc(50% - 56.25px);
    }
    form {
      padding: 0px 20px;
    }
  }
`

const InputsWrapper = styled.div`
  width: 100%;
  height: 100%;
  label {
    margin: 0px;
    padding: 0px;
    color: #724bb7;
    font-size: 1.5rem;
    font-weight: 500;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.3);
  }
  .inputField {
    margin: 0px 0px 10px 0px;
    padding: 5px 0px;
    width: 100%;
    color: #ff6584;
    font-size: 1.25rem;
    font-weight: 500;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.3);
    border-radius: 0px;
    border-top: none;
    border-right: none;
    border-bottom: 2px solid #724bb7;
    border-left: none;
    background: none;
  }
  #formMessage {
    margin-top: 5px;
    margin-bottom: 40px;
    font-family: roboto;
    font-weight: 400;
    font-size: 1.25rem;
    resize: none;
    border: 2px solid #724bb7;
    border-radius: 3px;
  }
  @media screen and (max-width: 800px) {
    label {
      font-size: 1.25rem;
    }
    .inputField {
      font-size: 1rem;
    }
    #formMessage {
      font-size: 1rem;
    }
  }
`

const Header = styled.p`
  margin: 0px 0px 10px 0px;
  padding: 0px;
  font-size: 50px;
  color: #724bb7;
  text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.5);
  @media screen and (max-width: 1566px) {
    margin: 0px auto 10px auto;
  }
  @media screen and (max-width: 700px) {
    width: 100%;
    font-size: 2.5rem;
    text-align: center;
  }
`

const SubHeader = styled.p`
  margin: 0px 0px 25px 0px;
  padding: 0px;
  font-size: 30px;
  color: #111;
  text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.3);
  @media screen and (max-width: 1566px) {
    margin: 0px auto 25px auto;
  }
  @media screen and (max-width: 700px) {
    padding: 0px 20px;
    width: 100%;
    font-size: 1.25rem;
    text-align: center;
  }
`

const DetailsContainer = styled.div`
  margin: 60px 15px 0px 50px;
  padding: 0px;
  min-width: 500px;
  max-width: 500px;
  position: relative;
  text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.3);
  .contactImg {
    border-radius: 3px;
    box-shadow: 0px 0px 3px 5px rgba(0, 0, 0, 0.1),
      0px 0px 5px 1.25px rgba(0, 0, 0, 0.5);
  }
  @media screen and (max-width: 1566px) {
    margin: 60px auto 120px auto;
    min-width: 0px;
    width: 500px;
    .contactImgContainer {
      margin: 0px auto;
    }
    p {
      text-align: center;
    }
    a {
      margin-left: calc(50% - 17.5px);
    }
  }
  @media screen and (max-width: 600px) {
    width: 100vw;
  }
`

// Styled-Components End

const Contact = () => {
  const [on, toggle] = useState(false)
  const animation = useSpring({
    opacity: on ? 1 : 0,
    config: config.slow,
  })
  return (
    <Container id="contact">
      {/* The contact id is there for the purpose of navigation */}
      <Banner>
        <p className="header">
          With over 25 years of experience, we bring the WOW factor to your
          event!
        </p>
        <p className="areaServed">
          DJ Sound Solutions is an award winning, full-time DJ, lighting and
          photo booth company that serves all of Florida, Indiana, Wisconsin and
          Illinois
        </p>
        <p className="travelFee">Travel fee may apply depending on location</p>
      </Banner>
      <div className="detailsFlexControl">
        <DetailsContainer>
          <Waypoint
            bottomOffset="30%"
            onEnter={() => {
              if (!on) toggle(true)
            }}
          >
            <animated.div style={animation}>
              <Sparky />
            </animated.div>
          </Waypoint>
          <p>Shawn Eickleberry: (863) 662-0259</p>
          <p>Christina Eickleberry: (815) 992-8776</p>
          <p>Email: DJSoundSolutions74@gmail.com</p>
          <a
            href="https://www.facebook.com/perfect.memories2017/"
            title="Visit us on Facebook"
            target="_blank"
            rel="noopener noreferrer"
          >
            {/* class fbIcon is defined in layout.css */}
            <IconContext.Provider value={{ className: "fbIcon" }}>
              <FaFacebook
                style={{
                  height: 35,
                  width: 35,
                }}
              />
            </IconContext.Provider>
          </a>
        </DetailsContainer>
        <ContactContainer>
          <Header>Let's Connect</Header>
          <SubHeader>Send us a message about your upcoming event</SubHeader>
          <form
            name="contact"
            method="post"
            data-netlify="true"
            data-netlify-honeypot="bot-field"
          >
            <InputsWrapper>
              <input
                type="hidden"
                name="form-name"
                value="contact"
                placeholder="Don’t fill this out if you're human"
              />
              <label htmlFor="formName">Name</label>
              <input
                name="name"
                type="text"
                placeholder="Your full name"
                className="inputField"
                id="formName"
                required
              />
              <label htmlFor="formEmail">Email</label>
              <input
                name="email"
                type="email"
                placeholder="Primary email address"
                className="inputField"
                id="formEmail"
              />
              <label htmlFor="formPhone">Phone</label>
              <input
                name="phone"
                type="text"
                placeholder="Best contact number"
                className="inputField"
                id="formPhone"
                required
              />
              <label htmlFor="formEvent">Event Type</label>
              <input
                name="event"
                type="text"
                placeholder="For example, wedding"
                className="inputField"
                id="formEvent"
              />
              <label htmlFor="formDate">Event Date</label>
              <Flatpickr
                name="date"
                type="text"
                placeholder="Please select your event date"
                className="inputField"
                id="formDate"
                style={{
                  marginBottom: 40,
                }}
              />
              <label htmlFor="formMessage">Message</label>
              <textarea
                rows="8"
                name="message"
                placeholder="Type the message here, then click send below"
                className="inputField"
                id="formMessage"
              />
              <br />
              <button type="submit">SEND</button>
            </InputsWrapper>
          </form>
        </ContactContainer>
      </div>
    </Container>
  )
}

export default Contact
