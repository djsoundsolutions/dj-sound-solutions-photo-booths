import React from "react"
import styled from "styled-components"
import { FaMoneyCheckAlt, FaBullseye, FaHandshake } from "react-icons/fa"
import { IconContext } from "react-icons"

// Styled-Components Start

const Container = styled.div`
  margin: -60px auto 20px 2.5%;
  padding: 18px;
  width: 95%;
  position: absolute;
  z-index: 2;
  border-radius: 3px;
  background: #fff;
  box-shadow: 0px 0px 3px 5px rgba(0, 0, 0, 0.1),
    0px 0px 5px 1.25px rgba(0, 0, 0, 0.5);
  @media screen and (max-width: 1030px) {
    display: none;
  }
`

const SubContainer = styled.div`
  margin: 0px;
  padding: 0px;
  width: 100%;
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
  flex-wrap: wrap;
  color: #f7f7f7;
  @media screen and (max-width: 1011px) {
    padding: 25px 0px 5px 0px;
    position: relative;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
  .moneyIcon {
    margin: 0px;
    padding: 0px;
    width: 75px;
    height: 75px;
    color: #724bb7;
    @media screen and (min-width: 1560px) and (max-width: 1792px) {
      width: 50px;
      height: 50px;
    }
    @media screen and (max-width: 1559px) {
      width: 45px;
      height: 45px;
    }
  }
  .bullseyeIcon {
    margin: 0px;
    padding: 0px;
    width: 65px;
    height: 65px;
    color: #724bb7;
    @media screen and (min-width: 1560px) and (max-width: 1792px) {
      width: 40px;
      height: 40px;
    }
    @media screen and (max-width: 1559px) {
      width: 40px;
      height: 40px;
    }
  }
  .handShakeIcon {
    margin: 0px;
    padding: 0px;
    width: 85px;
    height: 85px;
    color: #724bb7;
    @media screen and (min-width: 1560px) and (max-width: 1792px) {
      width: 60px;
      height: 60px;
    }
    @media screen and (max-width: 1559px) {
      width: 55px;
      height: 55px;
    }
  }
`

const Benefit = styled.div`
  padding: 0px;
  margin: 0px 20px 0px 20px;
  width: 29%;
  height: 80px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`

const BenefitHeading = styled.p`
  margin: 0px;
  padding: 0px 0px 0px 25px;
  font-size: 32px;
  color: #111;
  font-weight: bold;
  text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.3);
  @media screen and (min-width: 1560px) and (max-width: 1792px) {
    font-size: 1.75rem;
  }
  @media screen and (min-width: 1062px) and (max-width: 1559px) {
    font-size: 1.5rem;
  }
  @media screen and (max-width: 1061px) {
    font-size: 1.25rem;
  }
`

// Styled-Components End

const Benefits = () => {
  return (
    <div style={{ background: "#fff" }}>
      <Container>
        <SubContainer>
          {/* Benefit 1 */}
          <Benefit>
            <IconContext.Provider value={{ className: "moneyIcon" }}>
              <FaMoneyCheckAlt />
            </IconContext.Provider>
            <BenefitHeading>NO HIDDEN COSTS</BenefitHeading>
          </Benefit>
          {/* Benefit 2 */}
          <Benefit>
            <IconContext.Provider value={{ className: "handShakeIcon" }}>
              <FaHandshake />
            </IconContext.Provider>
            <BenefitHeading>RELIABLE</BenefitHeading>
          </Benefit>
          {/* Benefit 3 */}
          <Benefit>
            <IconContext.Provider value={{ className: "bullseyeIcon" }}>
              <FaBullseye />
            </IconContext.Provider>
            <BenefitHeading>DONE RIGHT</BenefitHeading>
          </Benefit>
          {/* End Benefits */}
        </SubContainer>
      </Container>
    </div>
  )
}

export default Benefits
