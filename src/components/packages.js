import React, { useState } from "react"
import { animated, useSpring, config } from "react-spring"
import { Waypoint } from "react-waypoint"
import styled from "styled-components"
import { FaCompactDisc } from "react-icons/fa"
import { IconContext } from "react-icons"
import Super from "../images/super.svg"

// Styled-Components Start

const Container = styled.section`
  margin: 0px;
  padding-top: 130px;
  position: relative;
  width: 100%;
  background: #fff;
  @media screen and (min-width: 1031px) and (max-width: 1700px) {
    padding-bottom: 100px;
  }
  @media screen and (min-width: 751px) and (max-width: 1030px) {
    padding-top: 50px;
    padding-bottom: 100px;
  }
  @media screen and (max-width: 750px) {
    padding-top: 50px;
    padding-bottom: 0px;
  }
  .super {
    margin: 0px;
    width: 300px;
    height: 300px;
    position: absolute;
    left: 65px;
    @media screen and (max-width: 1700px) {
      top: 100px;
    }
    @media screen and (max-width: 1400px) {
      display: none;
    }
  }
`

const PackagesContainer = styled.div`
  padding: 100px 40px 150px 40px;
  width: 100%;
  position: relative;
  display: flex;
  justify-content: space-around;
  .packagesHr {
    width: 200px;
    height: 5px;
    background: #ff6584;
    border: none;
    border-radius: 3px;
    position: absolute;
    top: 475px;
    left: 61px;
    @media screen and (min-width: 1834px) and (max-width: 1900px) {
      top: 420px;
      left: 83px;
    }
    @media screen and (min-width: 1801px) and (max-width: 1833px) {
      top: 450px;
      left: 83px;
    }
    @media screen and (min-width: 1787px) and (max-width: 1800px) {
      top: 450px;
      left: 73px;
    }
    @media screen and (min-width: 1701px) and (max-width: 1786px) {
      top: 485px;
      left: 73px;
    }
    @media screen and (max-width: 1700px) {
      display: none;
    }
  }
  @media screen and (max-width: 1700px) {
    padding: 20px 0px;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
  }
  @media screen and (max-width: 450px) {
    padding: 0px;
  }
  .packagesFlexWrapper {
    margin-top: 5px;
    width: 70%;
    display: flex;
    justify-content: space-around;
    align-items: center;
    @media screen and (min-width: 1801px) and (max-width: 1900px) {
      width: 75%;
    }
    @media screen and (min-width: 1701px) and (max-width: 1800px) {
      width: 77%;
    }
    @media screen and (min-width: 1401px) and (max-width: 1700px) {
      margin-bottom: 20px;
      padding: 25px;
      width: 100%;
      display: flex;
      flex-direction: row;
      justify-content: space-around;
      align-items: center;
    }
    @media screen and (min-width: 1301px) and (max-width: 1400px) {
      padding: 0px 25px;
      width: 100%;
      display: flex;
      justify-content: space-between;
    }
    @media screen and (min-width: 451px) and (max-width: 1300px) {
      padding: 25px 0px;
      width: 100%;
      height: 1500px;
      display: flex;
      flex-direction: column;
      justify-content: space-around;
      align-items: center;
    }
    @media screen and (max-width: 450px) {
      padding: 25px 0px;
      width: 100%;
      height: 1200px;
      display: flex;
      flex-direction: column;
      justify-content: space-around;
      align-items: center;
    }
  }
`

const PackageGradientBorder = styled.div`
  padding: 0px 2px 2px 2px;
  width: 404px;
  height: 365px;
  background: #724bb7;
  @media screen and (max-width: 450px) {
    width: 275px;
    height: 275px;
  }
`

const PackageContainer = styled.div`
  width: 400px;
  height: 350px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  background: #fff;
  box-shadow: 0 10px 20px rgba(0, 0, 0, 0.15), 0 3px 6px rgba(0, 0, 0, 0.1);
  .compactDiscPrem {
    margin-left: 25px;
    width: 45px;
    height: 45px;
    color: #724bb7;
  }
  .compactDiscPlat {
    margin-left: 25px;
    width: 45px;
    height: 45px;
    color: #e5e4e2;
  }
  .compactDiscPlatPlus {
    margin-left: 25px;
    padding: 5px;
    width: 55px;
    height: 55px;
    color: #e5e4e2;
    border: 2px solid #724bb7;
    border-radius: 50px;
  }
  @media screen and (max-width: 450px) {
    width: 271px;
    height: 260px;
  }
`

const PackageTitleDiv = styled.div`
  padding: 10px;
  position: relative;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  background-color: #111111;
  background-image: url("data:image/svg+xml,%3Csvg width='6' height='6' viewBox='0 0 6 6' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%23ffffff' fill-opacity='0.06' fill-rule='evenodd'%3E%3Cpath d='M5 0h1L0 6V5zM6 5v1H5z'/%3E%3C/g%3E%3C/svg%3E");
  @media screen and (max-width: 450px) {
    padding: 10px 10px 10px 0px;
  }
`

const PackageTitle = styled.p`
  margin: 0px 0px 0px 25px;
  padding: 5px;
  display: inline-block;
  font-size: 22px;
  color: #f7f7f7;
  text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.25);
  @media screen and (max-width: 450px) {
    margin: 0px 0px 0px 10px;
    font-size: 18px;
  }
`

const PackageDescDiv = styled.div`
  font-size: 18px;
  color: #111;
  text-align: left;
  font-weight: bold;
  text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.3);
  p {
    padding-left: 15px;
  }
  .PackageDescDivFinePrint {
    font-size: 14px;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.3);
    @media screen and (max-width: 450px) {
      font-size: 12px;
    }
  }  
  p {
    @media screen and (max-width: 450px) {
      padding-left: 15px;
      font-size: .70rem;
    }
  `

const Heading = styled.h2`
  margin: 0px 390px 0px auto;
  padding: 40px 20px 20px 20px;
  width: 576px;
  font-size: 65px;
  text-align: center;
  color: #724bb7;
  text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.5);
  @media screen and (min-width: 801px) and (max-width: 1700px) {
    margin-top: 0px;
    margin-bottom: 0px;
    margin-left: calc(50% - 288px);
  }
  @media screen and (min-width: 451px) and (max-width: 800px) {
    width: 100%;
    font-size: 3.5rem;
  }
  @media screen and (max-width: 450px) {
    width: 100%;
    font-size: 2.75rem;
  }
`

const BodyText = styled.p`
  padding: 20px;
  width: 30%;
  font-size: 32px;
  font-weight: bold;
  color: #111;
  line-height: 1.4;
  text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.3);
  @media screen and (min-width: 1701px) and (max-width: 1900px) {
    width: 20%;
    font-size: 1.5rem;
  }
  @media screen and (min-width: 1301px) and (max-width: 1700px) {
    padding: 10px 22.5%;
    width: 100%;
    text-align: center;
    .bodyTextBreak {
      display: none;
    }
  }
  @media screen and (min-width: 801px) and (max-width: 1300px) {
    padding: 10px 25px;
    width: 100%;
    text-align: center;
    .bodyTextBreak {
      display: none;
    }
  }
  @media screen and (min-width: 451px) and (max-width: 800px) {
    padding: 10px 25px;
    width: 100%;
    font-size: 1.25rem;
    text-align: center;
    .bodyTextBreak {
      display: none;
    }
  }
  @media screen and (max-width: 450px) {
    margin-top: 0px;
    padding: 10px 25px;
    width: 100%;
    font-size: 1rem;
    text-align: center;
    .bodyTextBreak {
      display: none;
    }
  }
`

// Styled-Components End

const Packages = () => {
  const [on, toggle] = useState(false)
  const [hrOn, toggleHr] = useState(false)
  const animation = useSpring({
    opacity: on ? 1 : 0,
    config: config.slow,
  })
  const hrAnimation = useSpring({
    width: hrOn ? "200px" : "0px",
    config: config.slow,
  })
  return (
    <Container id="packages">
      <Waypoint
        bottomOffset="30%"
        onEnter={() => {
          if (!on) toggle(true)
        }}
      >
        <animated.div style={animation}>
          <Super className="super" alt="heroic woman with cape" />
        </animated.div>
      </Waypoint>
      <Heading>DJ Packages</Heading>
      <PackagesContainer>
        <BodyText>
          It’s our goal to do more than just show up and play music.{" "}
          <br className="bodyTextBreak" />
          Anyone can do that! Choose us, because we will be there every step of
          the way going above and beyond to make your event just as you dreamed
          it would be.
        </BodyText>
        <Waypoint
          onEnter={() => {
            if (!hrOn) toggleHr(true)
          }}
        >
          <animated.hr style={hrAnimation} className="packagesHr" />
        </Waypoint>
        <div className="packagesFlexWrapper">
          {/* start section */}
          <PackageGradientBorder>
            <PackageContainer>
              <PackageTitleDiv style={{ padding: "15px 10px" }}>
                <IconContext.Provider value={{ className: "compactDiscPrem" }}>
                  <FaCompactDisc />
                </IconContext.Provider>
                <PackageTitle>Premium Package</PackageTitle>
              </PackageTitleDiv>
              <PackageDescDiv>
                <p style={{ color: "#724bb7" }}>4 Hours of Music</p>
                <p>Full DJ Booth</p>
                <p>Professional DJ/MC</p>
                <p>Speakers/Mics/Dance Floor Lighting</p>
                <p>Full Event Timeline Planning Form Provided</p>
                <p className="PackageDescDivFinePrint">(Reception Only)</p>
              </PackageDescDiv>
            </PackageContainer>
          </PackageGradientBorder>
          {/* end section */}
          {/* start section */}
          <PackageGradientBorder>
            <PackageContainer>
              <PackageTitleDiv style={{ padding: "15px 10px" }}>
                <IconContext.Provider value={{ className: "compactDiscPlat" }}>
                  <FaCompactDisc />
                </IconContext.Provider>
                <PackageTitle>Platinum Package</PackageTitle>
              </PackageTitleDiv>
              <PackageDescDiv>
                <p style={{ color: "#724bb7" }}>5 Hours of Music</p>
                <p>Full DJ Booth</p>
                <p>Professional DJ/MC</p>
                <p>Speakers/Mics/Dance Floor Lighting</p>
                <p>Full Event Timeline Planning Form Provided</p>
                <p className="PackageDescDivFinePrint">
                  (Cocktail and Reception Only)
                </p>
              </PackageDescDiv>
            </PackageContainer>
          </PackageGradientBorder>
          {/* end section */}
          {/* start section */}
          <PackageGradientBorder>
            <PackageContainer>
              <PackageTitleDiv>
                <IconContext.Provider
                  value={{ className: "compactDiscPlatPlus" }}
                >
                  <FaCompactDisc />
                </IconContext.Provider>
                <PackageTitle>Platinum Plus Package</PackageTitle>
              </PackageTitleDiv>
              <PackageDescDiv>
                <p style={{ color: "#724bb7" }}>6 Hours of Music</p>
                <p>Full DJ Booth</p>
                <p>Professional DJ/MC</p>
                <p>Speakers/Mics/Dance Floor Lighting</p>
                <p>Full Event Timeline Planning Form Provided</p>
                <p className="PackageDescDivFinePrint">
                  (Same Site Ceremony, Cocktail and Reception)
                </p>
              </PackageDescDiv>
            </PackageContainer>
          </PackageGradientBorder>
          {/* end section */}
        </div>
      </PackagesContainer>
    </Container>
  )
}

export default Packages
