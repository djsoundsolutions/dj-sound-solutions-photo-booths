import React, { useState } from "react"
import styled from "styled-components"
import { Link } from "gatsby"
import { FaBars } from "react-icons/fa"
import { IconContext } from "react-icons"

// Styled-Components Start

// Mobile Device Navbar Start

const MobileContainer = styled.div`
  margin: 0px;
  padding: 5px 0px;
  width: 100%;
  display: flex;
  justify-content: space-between;
  aling-items: center;
  position: fixed;
  overflow: hidden;
  top: 0px;
  z-index: 4;
  background: #fff;
  box-shadow: 0px 0px 3px 5px rgba(0, 0, 0, 0.1),
    0px 0px 5px 1.25px rgba(0, 0, 0, 0.6);
  h1 {
    margin-left: 35px;
    padding: 12.5px 0px 0px 0px;
    height: 100%;
    color: #724bb7;
    font-size: 1.5rem;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.5);
    @media screen and (min-width: 451px) and (max-width: 600px) {
      margin-right: 20px;
      margin-left: 17.5px;
      padding: 20px 0px 0px 0px;
      height: 100%;
      font-size: 1.2rem;
    }
    @media screen and (min-width: 384px) and (max-width: 450px) {
      margin-right: 20px;
      margin-left: 17.5px;
      padding: 20px 0px 0px 0px;
      width: 100%;
      height: 100%;
      font-size: 1rem;
    }
    @media screen and (min-width: 251px) and (max-width: 383px) {
      margin-right: 20px;
      margin-left: 17.5px;
      padding: 20px 15px 0px 0px;
      width: 100%;
      height: 100%;
      font-size: 0.9rem;
    }
    @media screen and (max-width: 250px) {
      margin-right: 20px;
      margin-left: 17.5px;
      padding: 0px 20px 0px 0px;
      width: 100%;
      height: 100%;
      font-size: 0.9rem;
    }
  }
  @media screen and (min-width: 1001px) {
    display: none;
  }
`

const HamburgerDiv = styled.div`
  margin: 7px 35px 0px 0px;
  padding: 5px;
  width: 50px;
  height: 50px;
  display: flex;
  justify-content: center;
  align-items: center;
  .barsIcon {
    padding: 0px;
    width: 100%;
    height: 100%;
    color: #724bb7;
    cursor: pointer;
  }
  @media screen and (max-width: 600px) {
    margin: 5px 35px 5px 15px;
  }
`

const MobileDropMenu = styled.ul`
  margin: 0px;
  padding: 20px 0px 5px 0px;
  position: fixed;
  top: 67px;
  z-index: 3;
  width: 100%;
  background: rgba(0, 0, 0, 0.9);
  list-style: none;
  @media screen and (min-width: 1001px) {
    display: none;
  }
  li {
    margin-bottom: 20px;
    width: 100%;
    text-align: center;
  }
  .mobileNavLinks {
    text-decoration: none;
    color: #fff;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.3);
    font-size: 1.5rem;
    font-weight: 400;
    border-bottom: 2px solid #724bb7;
    @media screen and (max-width: 400px) {
      font-size: 1rem;
    }
  }
`

// Mobile Device Navbar End

const Container = styled.div`
  margin: 0px;
  padding: 0px;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: relative;
  z-index: 2;
  background: #fff;
  box-shadow: 0px 0px 3px 5px rgba(0, 0, 0, 0.1),
    0px 0px 5px 1.25px rgba(0, 0, 0, 0.6);
  @media screen and (max-width: 1000px) {
    display: none;
  }
`

const Heading = styled.h1`
  margin: 0px 0px 0px 60px;
  padding: 10px;
  height: 100%;
  color: #724bb7;
  font-size: 38px;
  letter-spacing: 1px;
  text-shadow: 2.5px 1px 1px rgba(0, 0, 0, 0.5);
  @media screen and (min-width: 1593px) and (max-width: 1782px) {
    font-size: 2rem;
  }
  @media screen and (min-width: 1326px) and (max-width: 1592px) {
    font-size: 1.5rem;
  }
  @media screen and (min-width: 1124px) and (max-width: 1325px) {
    margin-left: 30px;
    font-size: 1.5rem;
  }
  @media screen and (min-width: 1060px) and (max-width: 1123px) {
    margin-left: 30px;
    font-size: 1.25rem;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.5);
  }
  @media screen and (max-width: 1059px) {
    margin-left: 30px;
    font-size: 1rem;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.5);
  }
`

const NavMenu = styled.ul`
  margin: 0px 60px 0px 0px;
  padding: 0px;
  width: 100%;
  list-style: none;
  text-align: center;
  .navListItems {
    border: 2px solid #fff;
    border-radius: 3px;
    @media screen and (min-width: 1301px) and (max-width: 1500px) {
      padding: 15px;
    }
    @media screen and (max-width: 1300px) {
      padding: 10px;
    }
  }
  .navListItems:hover {
    animation: linkAnimation 0.2s forwards;
  }
  .navLinks {
    text-decoration: none;
    color: #111;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.3);
    font-weight: 400;
    font-size: 1.5rem;
    border-bottom: 2px solid #724bb7;
    @media screen and (min-width: 1301px) and (max-width: 1749px) {
      font-size: 1.125rem;
    }
    @media screen and (max-width: 1300px) {
      font-size: 0.875rem;
    }
  }
`

const NavLi = styled.li`
  margin: 5px;
  padding: 20px;
  display: inline-block;
`

// Styled-Components End

const Navbar = () => {
  const [menuOn, setMenu] = useState(false)
  return (
    <header id="header">
      <MobileContainer>
        <a href="/" style={{ textDecoration: "none" }}>
          <Heading>DJ Sound Solutions & Photo Booths</Heading>
        </a>
        <HamburgerDiv
          onClick={() => {
            setMenu(!menuOn)
          }}
        >
          <IconContext.Provider value={{ className: "barsIcon" }}>
            <FaBars />
          </IconContext.Provider>
        </HamburgerDiv>
      </MobileContainer>
      <nav>
        <MobileDropMenu
          style={{
            animation: menuOn
              ? "mobileNavSlideDown .1s forwards"
              : "mobileNavSlideUp .1s forwards",
          }}
        >
          <li>
            <Link className="mobileNavLinks" to="#packages">
              WEDDING DJ PACKAGES
            </Link>
          </li>
          <li>
            <Link className="mobileNavLinks" to="#photobooth">
              PHOTO BOOTH
            </Link>
          </li>
          <li>
            <Link className="mobileNavLinks" to="/gallery">
              GALLERY
            </Link>
          </li>
          <li>
            <Link className="mobileNavLinks" to="/faq">
              FAQs
            </Link>
          </li>
          <li>
            <Link className="mobileNavLinks" to="#contact">
              CONTACT US
            </Link>
          </li>
        </MobileDropMenu>
      </nav>
      <Container>
        <a href="/" style={{ textDecoration: "none" }}>
          <Heading>DJ Sound Solutions & Photo Booths</Heading>
        </a>
        <nav>
          <NavMenu>
            <NavLi className="navListItems">
              <Link className="navLinks" to="#packages">
                WEDDING DJ PACKAGES
              </Link>
            </NavLi>
            <NavLi className="navListItems">
              <Link className="navLinks" to="#photobooth">
                PHOTO BOOTH
              </Link>
            </NavLi>
            <NavLi className="navListItems">
              <Link className="navLinks" to="/gallery">
                GALLERY
              </Link>
            </NavLi>
            <NavLi className="navListItems">
              <Link className="navLinks" to="/faq">
                FAQs
              </Link>
            </NavLi>
            <NavLi className="navListItems">
              <Link className="navLinks" to="#contact">
                CONTACT US
              </Link>
            </NavLi>
          </NavMenu>
        </nav>
      </Container>
    </header>
  )
}

export default Navbar
