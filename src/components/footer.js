import React from "react"
import styled from "styled-components"
import { FaFacebook } from "react-icons/fa"
import { IconContext } from "react-icons"

// Styled-Components Start

const Container = styled.div`
  margin: 0px;
  padding: 0px;
  width: 100%;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: flex-start;
  flex-wrap: wrap;
  background: #fff;
`

const Company = styled.div`
  margin: 10px 40px 10px 40px;
  padding: 25px 0px 25px 0px;
  .title {
    color: #724bb7;
    font-size: 28px;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.5);
    @media screen and (min-width: 601px) and (max-width: 1300px) {
      margin: 0px;
    }
    @media screen and (max-width: 600px) {
      margin: 0px;
      font-size: 22px;
    }
  }
  @media screen and (max-width: 1300px) {
    margin: 0px;
    padding: 25px;
    width: 100%;
    text-align: center;
    p {
      margin: 10px 0px 0px 0px;
    }
  }
`

const Services = styled.div`
  margin: 10px 40px 10px 40px;
  padding: 50px 0px;
  .title {
    color: #724bb7;
    font-size: 22px;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.5);
  }
  p {
    margin: 0px 0px 2px 0px;
    padding: 5px;
  }
  a {
    color: #111;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.3);
    text-decoration-color: #724bb7;
  }
  a:hover {
    text-decoration-color: #ff6584;
  }
  @media screen and (max-width: 1300px) {
    margin: 0px;
    padding: 20px;
    width: 100%;
    text-align: center;
    .title {
      margin: 0px;
    }
  }
`

const About = styled.div`
  margin: 10px 40px 10px 40px;
  padding: 50px 0px;
  .title {
    margin: 0px;
    color: #724bb7;
    font-size: 22px;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.5);
  }
  p {
    margin: 0px 0px 2px 0px;
    padding: 5px;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.3);
  }
  a {
    color: #111;
    text-decoration-color: #724bb7;
  }
  a:hover {
    text-decoration-color: #ff6584;
  }
  @media screen and (max-width: 1300px) {
    margin: 0px;
    padding: 20px;
    width: 100%;
    text-align: center;
  }
`

const Find = styled.div`
  margin: 10px 40px 10px 40px;
  padding: 60px 0px;
  @media screen and (max-width: 1300px) {
    margin: 0px;
    padding: 30px;
    width: 100%;
    text-align: center;
  }
`

// Styled-Components End

const Footer = () => {
  return (
    <footer>
      <hr
        style={{
          margin: "0px auto",
          width: "90%",
          height: 3,
          background: "#ff6584",
          border: "none",
          borderRadius: 3,
        }}
      />
      <Container>
        <Company>
          <a href="/#header" style={{ textDecoration: "none" }}>
            <p className="title">DJ Sound Solutions & Photo Booths</p>
          </a>
        </Company>
        <Services>
          <p className="title">Services</p>
          <p>
            <a href="/#packages">DJ Packages</a>
          </p>
          <p>
            <a href="/#photobooth">Photo Booth</a>
          </p>
        </Services>
        <About>
          <p className="title">About</p>
          <p>
            <a href="/#contact">Contact Us</a>
          </p>
          <p>
            <a href="/faq">FAQs</a>
          </p>
        </About>
        <Find>
          <a
            href="https://www.facebook.com/perfect.memories2017/"
            title="Visit us on Facebook"
            target="_blank"
            rel="noopener noreferrer"
          >
            {/* class fbIcon is defined in layout.css */}
            <IconContext.Provider value={{ className: "fbIcon" }}>
              <FaFacebook
                style={{
                  height: 35,
                  width: 35,
                }}
              />
            </IconContext.Provider>
          </a>
        </Find>
      </Container>
    </footer>
  )
}

export default Footer
