import React from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql } from "gatsby"
import "../css/layout.css"
import "typeface-roboto"

// This component wraps around everything. See index.js (pages directory)

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
            description
            author
          }
        }
      }
    `}
    render={data => <>{children}</>}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
