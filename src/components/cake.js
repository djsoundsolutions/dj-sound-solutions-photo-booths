import React from "react"
import { StaticQuery, graphql } from "gatsby"
import styled from "styled-components"
import Img from "gatsby-image"
import Party from "../images/party.svg"

const Container = styled.div`
  margin: 0px;
  padding: 50px 0px 150px 0px;
  width: 100%;
  display: flex;
  justify-content: space-around;
  align-items: center;
  @media screen and (min-width: 415px) and (max-width: 1663px) {
    padding: 50px 0px 150px 0px;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  @media screen and (max-width: 414px) {
    padding: 50px 0px 75px 0px;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .svgContainer {
    width: 500px;
    height: 500px;
    @media screen and (max-width: 1663px) {
      display: none;
    }
    .svg {
      width: 100%;
      height: 100%;
    }
  }
`

const CakeContainer = styled.div`
  margin: 0px;
  padding: 0px;
  width: 40%;
  border-radius: 3px;
  box-shadow: 0px 0px 3px 5px rgba(0, 0, 0, 0.1),
    0px 0px 5px 1.25px rgba(0, 0, 0, 0.5);
  @media screen and (min-width: 851px) and (max-width: 1663px) {
    min-width: 800px;
  }
  @media screen and (max-width: 850px) {
    width: 95%;
  }
  .cakeImg {
    border-radius: 3px;
  }
`

const Cake = () => (
  <StaticQuery
    query={graphql`
      query {
        placeholderImage: file(relativePath: { eq: "bride2011920.jpg" }) {
          childImageSharp {
            fluid(maxWidth: 3000) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    `}
    render={data => (
      <Container>
        <div className="svgContainer">
          <Party
            className="svg"
            alt="an animation of party guests toasting bottles"
          />
        </div>
        <CakeContainer>
          <Img
            className="cakeImg"
            fluid={data.placeholderImage.childImageSharp.fluid}
            alt="wedding cake"
          />
        </CakeContainer>
      </Container>
    )}
  />
)

export default Cake
