import React from "react"
import styled from "styled-components"

// Styled-Components Start

const Container = styled.section`
  margin: 0px;
  padding: 100px 0px;
  width: 100%;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: flex-start;
  @media screen and (max-width: 750px) {
    padding: 50px 0px;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
  }
`

const IncludedDivLeft = styled.div`
  margin: 0px;
  padding: 25px 100px 25px 25px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  @media screen and (min-width: 751px) and (max-width: 1100px) {
    padding: 25px;
    width: 45%;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
  }
  @media screen and (max-width: 750px) {
    padding: 25px;
    width: 90%;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: flex-start;
  }
`

const HzRule = styled.hr`
  margin: -35px 25px 0px 25px;
  width: 3px;
  height: 600px;
  background: #ff6584;
  border: none;
  border-radius: 3px;
  @media screen and (min-width: 1101px) and (max-width: 1321px) {
    margin: -25px 25px 0px 25px;
    height: 511px;
  }
  @media screen and (min-width: 751px) and (max-width: 1100px) {
    margin: -25px 25px 0px 25px;
    height: 400px;
  }
  @media screen and (max-width: 750px) {
    display: none;
  }
`

const IncludedDivRight = styled.div`
  margin: 0px;
  padding: 25px 25px 25px 100px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
  @media screen and (min-width: 751px) and (max-width: 1100px) {
    padding: 25px;
    width: 45%;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: center;
  }
  @media screen and (max-width: 750px) {
    margin-top: 50px;
    padding: 25px;
    width: 90%;
    display: flex;
    flex-direction: column;
    justify-content: space-around;
    align-items: flex-start;
  }
`

const IncludedHeadingLeft = styled.h3`
  margin: 0px 0px 15px 0px;
  padding: 0px;
  font-size: 42px;
  font-weight: bold;
  color: #724bb7;
  text-shadow: 1px 1px 1px rgba(2, 2, 2, 0.5);
  @media screen and (min-width: 1101px) and (max-width: 1321px) {
    font-size: 2.125rem;
  }
  @media screen and (min-width: 381px) and (max-width: 1100px) {
    font-size: 1.625rem;
    text-align: center;
  }
  @media screen and (max-width: 380px) {
    font-size: 1.2rem;
    text-align: left;
  }
`

const IncludedHeadingRight = styled.h3`
  margin: 0px 0px 20px 0px;
  padding: 0px;
  font-size: 42px;
  font-weight: bold;
  color: #724bb7;
  text-shadow: 1px 1px 1px rgba(2, 2, 2, 0.5);
  @media screen and (min-width: 1101px) and (max-width: 1321px) {
    font-size: 2.125rem;
  }
  @media screen and (min-width: 381px) and (max-width: 1100px) {
    font-size: 1.625rem;
    text-align: center;
  }
  @media screen and (max-width: 380px) {
    font-size: 1.2rem;
    text-align: left;
  }
`

const IncludedText = styled.p`
  margin: 0px 0px 10px 0px;
  padding: 0px;
  font-size: 32px;
  text-shadow: 1px 1px 1px rgba(2, 2, 2, 0.3);
  @media screen and (min-width: 1101px) and (max-width: 1321px) {
    font-size: 1.5rem;
  }
  @media screen and (max-width: 1100px) {
    font-size: 1rem;
  }
`

// Styled-Components End

const Extras = () => {
  return (
    <Container>
      <IncludedDivLeft>
        <IncludedHeadingLeft>Always Included</IncludedHeadingLeft>
        <IncludedText>Extra time for setup/breakdown</IncludedText>
        <IncludedText>Consultations as needed</IncludedText>
        <IncludedText>Backup emergency DJ</IncludedText>
        <IncludedText>Backup equipment</IncludedText>
      </IncludedDivLeft>
      {/* left section end */}
      {/* custom hr tag below */}
      <HzRule />
      <IncludedDivRight>
        <IncludedHeadingRight>Available Enhancements</IncludedHeadingRight>
        <IncludedText>Photo Booth!</IncludedText>
        <IncludedText>2 Different Style Booths to Choose From</IncludedText>
        <IncludedText>Premium Props Available</IncludedText>
        <IncludedText>Free Customization of Photo Strips</IncludedText>
        <IncludedText>Leather Memory Books</IncludedText>
        <IncludedText>USB Flash Drive</IncludedText>
        <IncludedText>Up Lighting</IncludedText>
        <IncludedText>Monogram</IncludedText>
        <IncludedText>Projector for Slideshows</IncludedText>
      </IncludedDivRight>
    </Container>
  )
}

export default Extras
