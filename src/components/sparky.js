import React from "react"
import { StaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

const Sparky = () => (
  <StaticQuery
    query={graphql`
      query {
        placeholderImage: file(
          relativePath: { eq: "sparkyAndGear011920.jpg" }
        ) {
          childImageSharp {
            fluid(maxWidth: 3000) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    `}
    render={data => (
      <div className="contactImgContainer" style={{ width: "75%" }}>
        <Img
          className="contactImg"
          fluid={data.placeholderImage.childImageSharp.fluid}
          alt="DJ Sparky"
        />
      </div>
    )}
  />
)

export default Sparky
