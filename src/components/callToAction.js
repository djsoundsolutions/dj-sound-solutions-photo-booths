import React from "react"
import styled from "styled-components"

// Styled-Components Start

const CTAContainer = styled.div`
  margin: 0px;
  padding: 40px 20px;
  width: 100%;
  height: 100%;
  position: relative;
  z-index: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  border-radius: 3px;
  background: rgba(68, 18, 115, 0.6);
  a {
    outline: 0;
  }
  @media screen and (max-width: 700px) {
    padding: 75px 20px 20px 20px;
    background: #42275a; /* fallback for old browsers */
    background: -webkit-linear-gradient(
      to top,
      #111,
      #42275a
    ); /* Chrome 10-25, Safari 5.1-6 */
    background: linear-gradient(
      to top,
      #111,
      #42275a
    ); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    border-radius: 0px;
    .ctaTextBreak {
      display: none;
    }
  }
`

const CTAText = styled.p`
  margin: 0px auto 20px auto;
  padding: 0px;
  color: #fff;
  font-weight: bold;
  font-size: 32px;
  letter-spacing: 0.5px;
  text-align: center;
  @media screen and (min-width: 574px) and (max-width: 1564px) {
    font-size: 1.5rem;
  }
  @media screen and (max-width: 573px) {
    font-size: 1.25rem;
  }
`

const CTASubText = styled.p`
  margin: 0px auto 20px auto;
  padding: 0px;
  color: #bfbdc1;
  font-size: 18px;
  text-align: center;
  @media screen and (min-width: 574px) and (max-width: 1564px) {
    font-size: 1rem;
  }
  @media screen and (max-width: 573px) {
    font-size: 0.9rem;
  }
`

const CTAButton = styled.button`
  margin: 20px auto 0px auto;
  padding: 15px;
  width: 225px;
  border: 3px solid #fff;
  border-radius: 0px;
  background: #b224aa;
  color: #fff;
  font-size: 20px;
  letter-spacing: 1.1px;
  text-shadow: 1px 1px 1px rgba(2, 2, 2, 0.5);
  :hover {
    animation: ctaButtonAnimation 0.2s forwards;
  }
`

// Styled-Components End

const CTA = () => {
  return (
    <CTAContainer>
      <CTAText>
        Don’t leave music to chance <br className="ctaTextBreak" /> when it
        comes to your wedding or special event
      </CTAText>
      <CTASubText>
        We've served over 1,000 weddings and celebrations, and we can’t wait{" "}
        <br className="ctaTextBreak" />
        to make your next big day an epic experience for you and your guests to
        enjoy.
      </CTASubText>
      <a href="#contact">
        <CTAButton>GET STARTED</CTAButton>
      </a>
    </CTAContainer>
  )
}

export default CTA
