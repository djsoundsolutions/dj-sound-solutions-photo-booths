import React from "react"
import { StaticQuery, graphql } from "gatsby"
import Img from "gatsby-image"

const PhotoBoothImg = () => (
  <StaticQuery
    query={graphql`
      query {
        placeholderImage: file(
          relativePath: { eq: "photoboothGuests011920.jpg" }
        ) {
          childImageSharp {
            fluid(maxWidth: 3000) {
              ...GatsbyImageSharpFluid
            }
          }
        }
      }
    `}
    render={data => (
      <div className="photoBooth">
        <Img
          className="photoBoothImg"
          fluid={data.placeholderImage.childImageSharp.fluid}
          alt="wedding photo booth"
        />
      </div>
    )}
  />
)

export default PhotoBoothImg
