module.exports = {
  siteMetadata: {
    title: `DJ Sound Solutions & Photo Booths | Trusted by 1,000+`,
    description: `Don’t leave music to chance when it comes to your wedding or special event. We've served over 1,000 weddings and celebrations, and we can’t wait
    to make your next big day an epic experience for you and your guests to enjoy.
    DJ Sound Solutions is an award winning, full-time DJ, lighting and photo booth company that serves all of Florida, Indiana, Wisconsin and Illinois
    `,
    author: `DJ Sound Solutions & Photo Booths`,
  },
  plugins: [
    {
      resolve: `gatsby-plugin-styled-components`,
      options: {
        // Add any options here
      },
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `DJ Sound Solutions & Photo Booths`,
        short_name: `DJ Sound Solutions & Photo Booths`,
        start_url: `/`,
        background_color: `#fff`,
        theme_color: `#724bb7`,
        display: `standalone`,
        icon: `src/images/dj.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-source-contentful`,
      options: {
        spaceId: process.env.SPACE_ID,
        // Learn about environment variables: https://gatsby.dev/env-vars
        accessToken: process.env.CONTENTFUL_ACCESS_TOKEN,
      },
    },
    {
      resolve: "gatsby-plugin-react-svg",
      options: {
        rule: {
          include: /images/,
        },
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: process.env.GOOGLE_ANALYTICS_TRACKING_ID,
        // Defines where to place the tracking script - `true` in the head and `false` in the body
        head: true,
        // Setting this parameter is optional
        anonymize: true,
        // Setting this parameter is also optional
        respectDNT: true,
        // Any additional create only fields (optional)
        cookieDomain: "auto",
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    "gatsby-plugin-offline",
  ],
}
